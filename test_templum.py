"""Templum automatical test script.
"""

__author__ = "Langston.Hakurei"
__version__ = "1.0"
__status__ = "Production"

import pytest
import requests
import json
import os


@pytest.fixture
def api_url(request):
    if os.getenv("TEST_URL") != None:
        return os.getenv("TEST_URL")
    else:
        return "http://localhost:5000"


def test_url_promote(api_url):
    print(f'"TEST_URL" -> {api_url}')
    assert True


def test_templum_docs(api_url):
    res = requests.get(api_url)
    code = res.status_code
    body = json.loads(res.text)
    if code == 200 and body["Templum-API"] == "v2":
        assert True
    else:
        assert False


def test_health_chcek(api_url):
    res = requests.get(api_url + "/health")
    code = res.status_code
    body = json.loads(res.text)
    if code == 200 and body["Application"] == "ok":
        assert True
    elif code == 503 and body["Application"] == "ok":
        assert True
    else:
        assert False
