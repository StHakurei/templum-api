# Templum-API

A RESTful-API application for my personal fun website [Templum](https://templum.sainthakurei.moe) and written in [Flask](https://flask.palletsprojects.com).


Database library: [PyMongo](https://pymongo.readthedocs.io).


CORS extension: [Flask-CORS](https://flask-cors.readthedocs.io).

## Docker deployment

### Build Docker image
Syntax: docker build --tag IMAGE_NAME:VERSION .


``` docker build --tag templumapi:1.0.0 . ```

### Run Docker image
Syntax: docker run --publish IP_ADDRESS:PORT:EXPOSE_PORT --detach (running in background) --name NAME (optional) IMAGE_NAME:VERSION


``` docker run --publish 127.0.0.1:8000:5000 --detach --name tapi-dev templum-api:1.0.0 ```


## Host deployment
Set database connect information. Or use IAM role for database connection when deploy on AWS.


```
ENV TEMPLUM_DB_URL=IP:PORT
ENV TEMPLUM_DB_USER=UUID
ENV TEMPLUM_DB_PWD=Password
```
