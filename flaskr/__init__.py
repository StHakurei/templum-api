"""Templum API.

This api is designed as backend service for Templum website.
"""
__author__ = "Langston.Hakurei"
__version__ = "3.0"
__status__ = "Production"

import os
import socket

from flask import Flask, jsonify
from flaskr.util import db_adapter
from flaskr.util import log_adapter
from flask_cors import CORS


class MissingParameterException(Exception):
    """Custom exception.

    Raise no params error.
    """

    pass


TEMPLUM_DB_URL = os.getenv("TEMPLUM_DB_URL")
TEMPLUM_DB_USER = os.getenv("TEMPLUM_DB_USER")
TEMPLUM_DB_PWD = os.getenv("TEMPLUM_DB_PWD")
# Retrieve database info from environments.


HOST = socket.gethostname()
# Retrieve container info.


if (TEMPLUM_DB_USER is None) or (TEMPLUM_DB_PWD is None):
    # Check necessary environments.
    raise MissingParameterException("Missing database environment.")


def create_app(test_config=None):
    """Create Templum API application.

    Note:
        Main module provide documents and health check function,
        and could running normally without proper database configuration.
    """
    api = Flask(__name__, instance_relative_config=True)
    CORS(api, methods=["GET"], origins=["https://templum.sainthakurei.moe", "*"])
    mongo = db_adapter.MongoAdapter(
        TEMPLUM_DB_URL, TEMPLUM_DB_USER, TEMPLUM_DB_PWD, "admin"
    )
    logs = log_adapter.LogAdapter()

    try:
        # Ensure the instance folder exists.
        os.makedirs(api.instance_path)
    except OSError:
        pass

    # Document service.
    @api.route("/", methods=["GET"])
    def service():
        service_list = {
            "TemplumAPI": "REST API for Templum website",
            "TemplumURL": "https://templum.sainthakurei.moe",
            "FateGO service": {
                "description": "fetch fatego gaming data.",
                "prefix": ".../fatego",
            },
            "Article service": {
                "description": "fetch article data.",
                "prefix": ".../article",
            },
        }
        return jsonify(service_list)

    # Health check service.
    @api.route("/health", methods=["GET"])
    def health_check():
        """Health check function.

        Note:
            This function is design to return health signal to common health check service,
            such as AWS Elastic Load Balancer, Target Group, etc.

        Returns:
            HTTP response code 200 for having database connection.
            HTTP response code 503 for without database connection.

            HTTP Body: Health chcek summary in JSON format.
        """
        health_report = {
            "TemplumAPI": "3.0",
            "ContainerId": HOST,
            "Application": "ok",
            "Database": "ok",
        }
        if mongo.connectTest():
            logs.addInfo("Sent health check signal.")
            return jsonify(health_report)
        else:
            health_report["Database"] = "lost"
            logs.addError("Lost database connection with {}.".format(TEMPLUM_DB_URL))
            return jsonify(health_report), 503

    # Import other service module.
    from . import fatego
    from . import article

    api.register_blueprint(fatego.fgo, url_prefix="/fatego")
    api.register_blueprint(article.arc, url_prefix="/article")

    return api
