"""Templum API.

Service log module.
"""
__author__ = "Langston.Hakurei"
__version__ = "3.0"
__status__ = "Production"

import datetime
import os


class LogAdapter(object):
    """Service log module.

    A simple log generate module.
    """

    def __init__(self):
        """Class constructor.

        Ensure log file is exists.
        """
        self.logFile = "./templumapi.log"
        if not os.path.isfile(self.logFile):
            try:
                logs = open(self.logFile, "w")
                logs.close()
            except OSError:
                pass

    def getTimestamp(self):
        """Time function.

        Returns:
            (str): timestamp
        """
        return str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))

    def addError(self, message):
        """Error log function.

        Add error record into log file.
        """
        with open(self.logFile, "a") as logs:
            logs.write("{} | ERROR | {}\n".format(self.getTimestamp(), str(message)))

    def addInfo(self, message):
        """Info log function.

        Add info record into log file.
        """
        with open(self.logFile, "a") as logs:
            logs.write("{} | INFO | {}\n".format(self.getTimestamp(), str(message)))
