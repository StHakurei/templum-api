"""Templum API.

Database adapter class for MongoDB.
"""
from bson.objectid import ObjectId
from pymongo import MongoClient
from pymongo.errors import ConnectionFailure
from pymongo.errors import OperationFailure
from .log_adapter import LogAdapter

__author__ = "Langston.Hakurei"
__version__ = "2.0"
__status__ = "Production"


class MongoAdapter(object):
    """Database connection class.

    This public class provide integrated access control and data process function
    for Templum API with MondoDB.
    """

    def __init__(self, url, uuid, passwd, auth_db):
        """Class constructor.

        Params:
            url (str): MongoDB connection url.
            uuid (str): Connection user.
            passwd (str): Connection password.
            auth_db (str): Authentication database.
        """
        self.uuid = uuid
        self.passwd = passwd
        self.auth_db = auth_db
        self.url = url
        self.logs = LogAdapter()

    def connectTest(self):
        """Database health check function.

        Returns:
            True - Sucessful retrieve database info.
            False - Fail to retrieve database info.
        """
        self.client = MongoClient(
            self.url,
            username=self.uuid,
            password=self.passwd,
            authSource=self.auth_db,
            authMechanism="SCRAM-SHA-1",
        )
        try:
            self.client.server_info()
            self.client.close()
            return True
        except Exception:
            return False

    def connect(self):
        """Initialize database connection.

        This class will not automatical initial database connection,
        before manipulate database, this function must be called first.
        """
        self.client = MongoClient(
            self.url,
            username=self.uuid,
            password=self.passwd,
            authSource=self.auth_db,
            authMechanism="SCRAM-SHA-1",
        )

    def getSummonData(self, event_id=None):
        """Retrieve summon data from database.

        Returns:
            parseSummonData()
        """
        try:
            return self.parseSummonData(self.client["fatego_usa"]["summon"], event_id)
        except ConnectionFailure as connError:
            self.logs.addError(connError)
            return None
        except OperationFailure as opsError:
            self.logs.addError(opsError)
            return None
        except Exception as badRequest:
            self.logs.addError(badRequest)
            return None

    def getServantData(self, ingame_id=None):
        """Retrieve servant origin from database.

        Returns:
            parseServantData()
        """
        try:
            return self.parseServantData(
                self.client["fatego_usa"]["servant"], ingame_id
            )
        except ConnectionFailure as connError:
            self.logs.addError(connError)
            return None
        except OperationFailure as opsError:
            self.logs.addError(opsError)
            return None
        except Exception as badRequest:
            self.logs.addError(badRequest)
            return None

    def getArticleData(self, article_id=None):
        """Retrieven article from database.

        Returns:
            parseArticleData()
        """
        try:
            return self.parseArticleData(self.client["article"]["archive"], article_id)
        except ConnectionFailure as connError:
            self.logs.addError(connError)
            return None
        except OperationFailure as opsError:
            self.logs.addError(opsError)
            return None
        except Exception as badRequest:
            self.logs.addError(badRequest)
            return None

    def parseSummonData(self, cursor, event_id=None):
        """Encapsulate summon data.

        Returns:
            An array of dict, if sucessful retrieven data from database.
            None, if fail to encapsulate data.
        """
        collection = []
        if event_id is None:
            for doc in cursor.find():
                json_format = {
                    "id": str(doc["_id"]),
                    "iconId": doc["iconId"],
                    "event": doc["event"],
                    "pack": doc["pack"],
                    "sq": doc["saintQuartz"],
                    "ticket": doc["ticket"],
                    "summoning": doc["summoning"],
                    "servant": doc["servant"],
                    "craftEssences": doc["craftEssences"],
                }
                collection.append(json_format)
        else:
            try:
                doc = cursor.find_one({"_id": ObjectId(event_id)})
                if doc is not None:
                    json_format = {
                        "id": str(doc["_id"]),
                        "iconId": doc["iconId"],
                        "event": doc["event"],
                        "pack": doc["pack"],
                        "sq": doc["saintQuartz"],
                        "ticket": doc["ticket"],
                        "summoning": doc["summoning"],
                        "servant": doc["servant"],
                        "craftEssences": doc["craftEssences"],
                    }
                    collection.append(json_format)
                else:
                    return None
            # Raise badRequest exception when data parsing failed.
            except Exception as badRequest:
                self.logs.addError(badRequest)
                return None
        return collection

    def parseServantData(self, cursor, ingame_id=None):
        """Encapsulate servant data.

        Returns:
            An array of dict, if sucessful retrieven data from database.
            None, if fail to encapsulate data.
        """
        collection = []
        if ingame_id is None:
            for doc in cursor.find():
                json_format = {
                    "ingame_id": doc["ingameId"],
                    "name": doc["name"],
                    "class": doc["class"],
                    "rarity": doc["rarity"],
                    "wiki_url": doc["wikiUrl"],
                }
                collection.append(json_format)
        else:
            try:
                doc = cursor.find_one({"ingameId": ingame_id})
                if doc is not None:
                    json_format = {
                        "ingame_id": doc["ingameId"],
                        "name": doc["name"],
                        "class": doc["class"],
                        "rarity": doc["rarity"],
                        "wiki_url": doc["wikiUrl"],
                    }
                    collection.append(json_format)
                else:
                    return None
            # Raise badRequest exception when data parsing failed.
            except Exception as badRequest:
                self.logs.addError(badRequest)
                return None
        return collection

    def parseArticleData(self, cursor, article_id=None):
        """Encapsulate article data.

        Returns:
            An array of dict, if sucessful retrieven data from database.
            None, if fail to encapsulate data.
        """
        collection = []
        if article_id is None:
            for doc in cursor.find():
                json_format = {
                    "id": str(doc["_id"]),
                    "title": doc["title"],
                    "author": doc["author"],
                    "create_time": doc["createTime"],
                    "has_cover": doc["hasCover"],
                    "cover": doc["coverImage"],
                    "tag": doc["tag"],
                }
                collection.append(json_format)
        else:
            doc = cursor.find_one({"_id": ObjectId(article_id)})
            if doc is not None:
                json_format = {
                    "id": str(doc["_id"]),
                    "title": doc["title"],
                    "author": doc["author"],
                    "create_time": doc["createTime"],
                    "has_cover": doc["hasCover"],
                    "cover": doc["coverImage"],
                    "tag": doc["tag"],
                    "article_content": doc["article"],
                }
                collection.append(json_format)
            else:
                return None
        return collection
