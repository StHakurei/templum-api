"""Templum API.

FateGO module.
"""
__author__ = "Langston.Hakurei"
__version__ = "3.0"
__status__ = "Production"

import os

from flask import Blueprint
from flask import jsonify
from flaskr.util import db_adapter


TEMPLUM_DB_URL = os.getenv("TEMPLUM_DB_URL")
TEMPLUM_DB_USER = os.getenv("TEMPLUM_DB_USER")
TEMPLUM_DB_PWD = os.getenv("TEMPLUM_DB_PWD")
# Retrieve database info from environments.

fgo = Blueprint("fatego", __name__)
mongo = db_adapter.MongoAdapter(
    TEMPLUM_DB_URL, TEMPLUM_DB_USER, TEMPLUM_DB_PWD, "admin"
)
mongo.connect()
# Create FateGO module, initial database connection, implement CORS.


@fgo.route("/", methods=["GET"])
def module_info():
    """Fatego module document service.

    Returns:
       JSON format content.
    """
    URLs = {
        ".../servants": {
            "description": "retrieve all servant records.",
            "content-tyep": "application/json",
            "accept-method": "GET",
        },
        ".../servants/<in_game_id>": {
            "description": "retrieve one servant record.",
            "content-tyep": "application/json",
            "accept-method": "GET",
        },
        ".../summons": {
            "description": "retrieve all summon records.",
            "content-tyep": "application/json",
            "accept-method": "GET",
        },
        ".../summons/<event_id>": {
            "description": "retrieve one summon record.",
            "content-tyep": "application/json",
            "accept-method": "GET",
        },
    }
    return jsonify(URLs)


@fgo.route("/servants", methods=["GET"])
def get_servants():
    """Servant information service.

    Note:
        This service return all servant information.

    Returns:
        JSON format content.
    """
    return jsonify(mongo.getServantData())


@fgo.route("/servants/<int:ingame_id>", methods=["GET"])
def get_servant(ingame_id):
    """Single servant origin query service.

    Note:
        Used to retrieve specific servant information.

    Params:
        ingame_id (int): The servant id in Fate Grand Order.

    Returns:
        JSON format content.

    .. ingame_id:
        https://grandorder.wiki/Servant_List
    """
    return jsonify(mongo.getServantData(ingame_id))


@fgo.route("/summons", methods=["GET"])
def get_summons():
    """Summon records service.

    Note:
        This service return all summons records with detail.

    Returns:
        JSON format content.
    """
    return jsonify(mongo.getSummonData())


@fgo.route("/summons/<string:event_id>", methods=["GET"])
def get_summon(event_id):
    """Single summons records query service.

    Note:
        Used to retrieve specific summon record by event_id.
        Used get_summons() function to get event_id.

    Params:
        event_id (str): The summon record id in database.

    Returns:
        JSON format content.

    """
    return jsonify(mongo.getSummonData(event_id))
