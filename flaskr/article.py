"""Templum API.

Article module.
"""
__author__ = "Langston.Hakurei"
__version__ = "3.0"
__status__ = "Production"

import os

from flask import Blueprint
from flask import jsonify
from flaskr.util import db_adapter


TEMPLUM_DB_URL = os.getenv("TEMPLUM_DB_URL")
TEMPLUM_DB_USER = os.getenv("TEMPLUM_DB_USER")
TEMPLUM_DB_PWD = os.getenv("TEMPLUM_DB_PWD")
# Retrieve database info from environments.

arc = Blueprint("article", __name__)
mongo = db_adapter.MongoAdapter(
    TEMPLUM_DB_URL, TEMPLUM_DB_USER, TEMPLUM_DB_PWD, "admin"
)
mongo.connect()
# Create Article module, initial database connection, implement CORS.


@arc.route("/", methods=["GET"])
def module_info():
    """Article module document service.

    Returns:
       JSON format content.
    """
    URLs = {
        ".../archive": {
            "description": "retrieve article list.",
            "content-tyep": "application/json",
            "accept-method": "GET",
        },
        ".../archive/<archive_id>": {
            "description": "retrive article content",
            "content-tyep": "application/json",
            "accept-method": "GET",
        },
    }
    return jsonify(URLs)


@arc.route("/archive", methods=["GET"])
def get_article_list():
    """Article list service.

    Note:
        This service return a list of all available articles.
        The list contains blow attributes:
            - article_id
            - title
            - author
            - tag
        Use article_id to retrieve article content by get_article() function.

    Returns:
        JSON format content.
    """
    return jsonify(mongo.getArticleData())


@arc.route("/archive/<string:article_id>", methods=["GET"])
def get_article(article_id):
    """Article content service.

    Note:
        This service is used to retrieve specific article content.

    Args:
        article_id (str): Article id in database.

    Returns:
        JSON format content.
    """
    return jsonify(mongo.getArticleData(article_id))
