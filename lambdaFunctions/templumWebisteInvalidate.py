"""Templum API - AWS Lambda script.

This lambda function is used to work with AWS S3 Events.
When new object upload to S3 bucket, this function will be trigger
to create an invalidation process in perspective CloudFront distribution.

Note:
   This lambda function requires below IAM policies.
   - ses:SendEmail
   - logs:CreateLogStream
   - logs:CreateLogGroup
   - logs:PutLogEvents
   - cloudfront:CreateInvalidation

"""

__author__ = "Langston.Hakurei"
__version__ = "3.0"
__status__ = "Production"

import datetime
import os
import boto3
import json
from botocore.exceptions import ClientError


class MissingParameterException(Exception):
    """Custom exception class.

    Raise error to trigger Lambda function failure.
    """

    pass


functionName = os.getenv("FUNCTION_NAME")
distributionId = os.getenv("DISTRIBUTION_ID")
sender = os.getenv("VERIFIED_SENDER")
recipient = os.getenv("NOTIFICATION_MAIL")
"""Retrieve necessary params from environment variables.

Function Name used for analysis.
CloudFront distribution id.
Verified sender email address in AWS SES.
Verified recevier email address in AWS SES.
"""

CHARSET = "UTF-8"
SUBJECT = "AWS Lambda Message (Failure)"


def parseNotificationMail(service, function, detail):
    """Notification content template in HTML.

    Args:
        service (str): Service group which this lambda function belongs to.
        function (str): Application which this lambda function runs as.
        detail (str): Error message body.

    Returns:
        HTML document in string.
    """
    return f"""<html>
    <head></head>
    <body>
    <h1>Task Failure Report</h1>
    <h4>Service: {service}</h3>
    <h4>Application: {function}</h3>
    <p>Detail: {detail}</p>
    </body>
    </html>
    """


def lambda_handler(event, context):
    """Lambda handler.

    Note:
        Try to create an invalidation in given CloudFront distribution,
        if the creation is failed, send notification mail to recipient.

    Raise:
        MissingParamterException: If params are not give, trigger Lambda function failure.
        ClientError: If send notification mail is failed, trigger Labmda function failure.
    """
    if functionName is None:
        raise MissingParameterException("*Missing Environment: FUNCTION_NAME")

    if sender is None:
        raise MissingParameterException("*Missing Environment: VERIFIED_SENDER")

    if recipient is None:
        raise MissingParameterException("*Missing Environment: NOTIFICATION_MAIL")

    if distributionId is None:
        raise MissingParameterException("*Unsigned distribution id")

    try:
        client = boto3.client("cloudfront")
        client.create_invalidation(
            DistributionId=distributionId,
            InvalidationBatch={
                "Paths": {"Quantity": 1, "Items": ["/*"]},
                "CallerReference": functionName
                + " - "
                + str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")),
            },
        )
        return json.dumps({"result": "successful"})
    except ClientError as error:
        ses = boto3.client("ses", "us-east-1")
        ses.send_email(
            Destination={"ToAddresses": [recipient]},
            Message={
                "Body": {
                    "Html": {
                        "Charset": CHARSET,
                        "Data": parseNotificationMail(
                            "Templum", functionName, str(error)
                        ),
                    }
                },
                "Subject": {"Charset": CHARSET, "Data": SUBJECT},
            },
            Source=sender,
        )
        return json.dumps(
            {
                "result": "failed",
                "timestamp": str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")),
                "message": str(error),
            }
        )
