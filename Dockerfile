FROM python:3.8-alpine

WORKDIR /usr/src/app

COPY requirements.txt ./

RUN pip install --no-cache-dir -r requirements.txt

COPY ./flaskr ./flaskr

# Uncomment and modify below environments to connect to dev database.
# ENV TEMPLUM_DB_URL=mongodb://192.168.50.106:27017/
# ENV TEMPLUM_DB_USER=developer001
# ENV TEMPLUM_DB_PWD=password001

EXPOSE 5000

ENV FLASK_APP=flaskr
ENV FLASK_ENV=Production
# Set 'Development' in program development.
# ENV FLASK_ENV=Development

CMD ["flask", "run", "--host=0.0.0.0"]
